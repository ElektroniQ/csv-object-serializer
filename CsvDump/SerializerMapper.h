#include <string>
#include <map>
#include <vector>

template <class T>
class SerializerMapper
{
private:
	std::vector <std::string> ordering;
	std::map<std::string, long T::*> longsMap;
	std::map<std::string, int T::*> intsMap;
	std::map<std::string, double T::*> doublesMap;
	std::map<std::string, float T::*> floatsMap;
	std::map<std::string, std::string T::*> stringsMap;

	T* struct_obj;

	int iter;

public:
	template <class T_member>
	SerializerMapper<T>& MapField(std::string name, T_member T::* field);

	void* GetField(T& entity, std::string name);
	std::string GetString(const T& entity, std::string name);

	SerializerMapper<T>& SetField(T& entity, std::string name, std::string value);

	void Clear();

	std::vector<std::string> GetKeys() const
	{
		auto ret = ordering;
		return ret;
	}

	std::vector<std::string>::iterator begin()
	{
		return ordering.begin();
	};

	std::vector<std::string>::iterator end()
	{
		return ordering.end();
	};

private:
	void Emplace(std::string name, long T::* field);
	void Emplace(std::string name, int T::* field);
	void Emplace(std::string name, double T::*  field);
	void Emplace(std::string name, float T::* field);
	void Emplace(std::string name, std::string T::* field);
};

template <class T>
template <class T_member>
SerializerMapper<T>& SerializerMapper<T>::MapField(std::string name, T_member T::* field)
{
	this->Emplace(name, field);
	return *this;
}


template <class T>
void* SerializerMapper<T>::GetField(T& entity, std::string name)
{
	if (longsMap.count(name) > 0)
	{
		auto member_ptr = this->longsMap[name];
		return (void*)&(entity.*member_ptr);
	}

	if (intsMap.count(name) > 0)
	{
		auto member_ptr = this->intsMap[name];
		return (void*) & (entity.*member_ptr);
	}

	if (doublesMap.count(name) > 0)
	{
		auto member_ptr = this->doublesMap[name];
		return (void*) & (entity.*member_ptr);
	}

	if (floatsMap.count(name) > 0)
	{
		auto member_ptr = this->floatsMap[name];
		return (void*) & (entity.*member_ptr);
	}

	if (stringsMap.count(name) > 0)
	{
		auto member_ptr = this->stringsMap[name];
		return (void*) & (entity.*member_ptr);
	}

	return NULL;
}

template <class T>
SerializerMapper<T>& SerializerMapper<T>::SetField(T& entity, std::string name, std::string value)
{
	if (longsMap.count(name) > 0)
	{
		auto member_ptr = this->longsMap[name];
		entity.*member_ptr = std::stoi(value);
	}

	if (intsMap.count(name) > 0)
	{
		auto member_ptr = this->intsMap[name];
		entity.*member_ptr = std::stoi(value);
	}

	if (doublesMap.count(name) > 0)
	{
		auto member_ptr = this->doublesMap[name];
		entity.*member_ptr = std::stod(value);
	}

	if (floatsMap.count(name) > 0)
	{
		auto member_ptr = this->floatsMap[name];
		entity.*member_ptr = std::stod(value);
	}

	if (stringsMap.count(name) > 0)
	{
		auto member_ptr = this->stringsMap[name];
		entity.*member_ptr = value;
	}

	return *this;
}

template <class T>
std::string SerializerMapper<T>::GetString(const T& entity, std::string name)
{
	if (longsMap.count(name) > 0)
	{
		auto member_ptr = this->longsMap[name];
		return std::to_string(entity.*member_ptr);
	}

	if (intsMap.count(name) > 0)
	{
		auto member_ptr = this->intsMap[name];
		return std::to_string(entity.*member_ptr);
	}

	if (doublesMap.count(name) > 0)
	{
		auto member_ptr = this->doublesMap[name];
		return std::to_string(entity.*member_ptr);
	}

	if (floatsMap.count(name) > 0)
	{
		auto member_ptr = this->floatsMap[name];
		return std::to_string(entity.*member_ptr);
	}

	if (stringsMap.count(name) > 0)
	{
		auto member_ptr = this->stringsMap[name];
		return entity.*member_ptr;
	}

	return "";
}

template <class T>
void SerializerMapper<T>::Clear()
{
	ordering.clear();
	longsMap.clear();
	intsMap.clear();
	doublesMap.clear();
	floatsMap.clear();
	stringsMap.clear();
	struct_obj = NULL;
}

template <class T>
void SerializerMapper<T>::Emplace(std::string name, long T::* field)
{
	ordering.push_back(name);
	this->longsMap[name] = field;
}

template <class T>
void SerializerMapper<T>::Emplace(std::string name, int T::* field)
{
	ordering.push_back(name);
	this->intsMap[name] = field;
}

template <class T>
void SerializerMapper<T>::Emplace(std::string name, double T::* field)
{
	ordering.push_back(name);
	this->doublesMap[name] = field;
}

template <class T>
void SerializerMapper<T>::Emplace(std::string name, float T::* field)
{
	ordering.push_back(name);
	this->floatsMap[name] = field;
}

template <class T>
void SerializerMapper<T>::Emplace(std::string name, std::string T::* field)
{
	ordering.push_back(name);
	this->stringsMap[name] = field;
}
