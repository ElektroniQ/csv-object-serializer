#include <iostream>
#include <sstream>
#include "CsvSerializer.h"
#include <functional>

struct Test
{
	double double_val;
	int int_val;
	std::string str_val;
};

template<class T>
class BufferBase
{
public:
	virtual void Add(const T& item) = 0;
	virtual void Flush() = 0;
	virtual std::pair<T, bool> FindFirst(std::function<bool(const T & item, size_t pos)> filter, size_t start_pos = 0) = 0;
	virtual std::pair<T, bool>  ReverseFindFirst(std::function<bool(const T & item, size_t pos)> filter, size_t start_pos = 0) = 0;
	virtual std::vector<T> FindAll(std::function<bool(const T & item, size_t pos)> filter, size_t start_pos = 0) = 0;
};

template<class T>
class CsvFileBuffer
	:
	BufferBase<T>
{
private:
	std::vector<std::string> items;
	CsvSerializer<T> serializer;
public:
	CsvFileBuffer();
	void SetMapping(SerializerMapper<T> mapping);
	virtual void Add(const T& item);
	virtual void Flush();
	virtual std::pair<T, bool> FindFirst(std::function<bool(const T & item, size_t pos)> filter, size_t start_pos=0);
	virtual std::pair<T, bool> ReverseFindFirst(std::function<bool(const T & item, size_t pos)> filter, size_t start_pos=0);
	virtual std::vector<T> FindAll(std::function<bool(const T & item, size_t pos )> filter, size_t start_pos=0);
	~CsvFileBuffer();
};

template<class T>
CsvFileBuffer<T>::CsvFileBuffer()
{

}

template<class T>
void CsvFileBuffer<T>::SetMapping(SerializerMapper<T> mapping)
{
	serializer.SetMapper(mapping);
}

template<class T>
void CsvFileBuffer<T>::Add(const T& item)
{
	items.push_back(serializer.ToString(item));
}

template<class T>
void CsvFileBuffer<T>::Flush()
{

}

template<class T>
std::pair<T, bool> CsvFileBuffer<T>::FindFirst(std::function<bool(const T & item, size_t pos)> filter, size_t start_pos)
{
	for (size_t i = start_pos; i<items.size(); ++i)
	{
		auto object = serializer.FromString(items[i]);
		if (filter(object, i))
			return std::make_pair(object, true);
	}

	return std::make_pair(T(), false);
}

template<class T>
std::pair<T, bool> CsvFileBuffer<T>::ReverseFindFirst(std::function<bool(const T & item, size_t pos)> filter, size_t start_pos)
{
	for (size_t i = items.size() - start_pos - 1; i >0; --i)
	{
		auto object = serializer.FromString(items[i]);
		if (filter(object, i))
			return std::make_pair(object, true);
	}

	return std::make_pair(T(), 0);
}

template<class T>
std::vector<T> CsvFileBuffer<T>::FindAll(std::function<bool(const T & item, size_t pos)> filter, size_t start_pos)
{
	std::vector<T> ret;
	for (size_t i = start_pos; i < items.size(); ++i)
	{
		auto object = serializer.FromString(items[i]);
		if (filter(object, i))
			ret.push_back(object);
	}
	return ret;
}

template<class T>
CsvFileBuffer<T>::~CsvFileBuffer()
{

}


int main(void)
{
	auto mapper = SerializerMapper<Test>()
		.MapField("double_val", &Test::double_val)
		.MapField("int_val", &Test::int_val)
		.MapField("str_val", &Test::str_val);

	CsvSerializer<Test> testSerializer(mapper);

	CsvFileBuffer<Test> buffer;
	buffer.SetMapping(mapper);

	for (auto i = 0; i < 10; ++i)
	{
		Test a;
		a.int_val = i;
		a.double_val = i*2.0;
		a.str_val = "aaaaa";

		buffer.Add(a);
	}

	auto obj = buffer.FindAll(
		[](const Test& item, const size_t pos) -> bool
		{
			if (item.double_val > 3.5 and item.int_val < 5)
				return true;
			return false;
		});

	std::cout << testSerializer.GetHeader() << std::endl;
	for(auto& it: obj)
		std::cout << testSerializer.ToString(it) << std::endl;

	return 0;
}