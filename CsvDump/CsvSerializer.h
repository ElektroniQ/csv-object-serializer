#include "SerializerMapper.h"

template<class T>
class CsvSerializer
{
private:
	SerializerMapper<T> mapper;
public:
	CsvSerializer<T>();
	CsvSerializer<T>(SerializerMapper<T> mapper);

	CsvSerializer<T> SetMapper(SerializerMapper<T> mapper);

	std::string GetHeader();

	std::string ToString(const T& object);

	T FromString(std::string str);


private:
	std::vector<std::string> Split(const char* str, char c = ';') const;
};

template<class T>
CsvSerializer<T>::CsvSerializer<T>()
{
}

template<class T>
CsvSerializer<T>::CsvSerializer<T>(SerializerMapper<T> mapper)
{
	this->mapper = mapper;
}

template<class T>
CsvSerializer<T> CsvSerializer<T>::SetMapper(SerializerMapper<T> mapper)
{
	this->mapper = mapper;
	return *this;
};

template<class T>
std::string CsvSerializer<T>::GetHeader()
{
	std::ostringstream ss;
	for (auto& field : mapper)
	{
		ss << field << ';';
	}
	return ss.str();
}

template<class T>
std::string CsvSerializer<T>::ToString(const T& object)
{
	std::ostringstream ss;
	for (auto& field : mapper)
	{
		ss << mapper.GetString(object, field) << ';';
	}

	return ss.str();
};

template<class T>
T CsvSerializer<T>::FromString(std::string str)
{
	T object;
	auto tokens = Split(str.c_str());
	auto keys = mapper.GetKeys();
	for (int i = 0; i < tokens.size() - 1; i++)
	{
		mapper.SetField(object, keys[i], tokens[i]);
	}
	return object;
}

template<class T>
std::vector<std::string> CsvSerializer<T>::Split(const char* str, char c) const
{
	std::vector<std::string> result;

	do
	{
		const char* begin = str;

		while (*str != c && *str)
			str++;

		result.push_back(std::string(begin, str));
	} while (0 != *str++);

	return result;
}